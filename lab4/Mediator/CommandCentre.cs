
namespace Lab4.Mediator
{
  class CommandCentre : IMediator
  {
    private Dictionary<string, Runway> _runways;
    private Dictionary<string, Aircraft> _aircrafts;

    public CommandCentre(Dictionary<string, Runway> runways, Dictionary<string, Aircraft> aircrafts)
    {
      _runways = runways ?? new();
      _aircrafts = aircrafts ?? new();

      foreach (var el in _runways)
        el.Value.SetMediator(this);

      foreach (var el in _aircrafts)
        el.Value.SetMediator(this);
    }

    public void Notify(Request request)
    {
      switch (request.command)
      {

        case "BusyRunway":
          BusyRunway(request);
          break;
        case "TakeOffRunway":
          TakeOffRunway(request);
          break;

      }
    }

    private void BusyRunway(Request request)
    {
      var runwayId = request.requestData.ToString();
      var runway = _runways[runwayId];
      runway.IsBusyWithAircraft = true;
      runway.HighLightRed();
    }

    private void TakeOffRunway(Request request)
    {
      var runwayId = request.requestData.ToString();
      var runway = _runways[runwayId];
      runway.IsBusyWithAircraft = false;
      runway.HighLightGreen();
    }

  }
}