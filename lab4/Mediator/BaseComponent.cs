namespace Lab4.Mediator;
class BaseComponent
{
    protected IMediator _mediator;

    public void SetMediator(IMediator mediator)
    {
        this._mediator = mediator;
    }
}