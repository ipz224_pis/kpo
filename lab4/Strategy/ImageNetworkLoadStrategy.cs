public class ImageNetworkLoadStrategy : IImagesStrategy
{
    public string Execute(string href)
    {
        return $"Render img({href}) from network";
    }
}