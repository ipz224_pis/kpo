public interface IImagesStrategy
{
    string Execute(string href);
}