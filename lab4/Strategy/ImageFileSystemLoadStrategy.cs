public class ImageFileSystemLoadStrategy : IImagesStrategy
{
    public string Execute(string href)
    {
        return $"Render img({href}) from file system";
    }
}