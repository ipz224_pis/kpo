public class SubscriberBeforeRender : ISubscriber
{
    public void Notify(object eventData)
    {
        Console.WriteLine($"type of element before render: {eventData.GetType()}");
    }
}