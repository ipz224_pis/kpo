namespace Lab4.ChainOfResponsibility;

public interface IHandler<T, K>
{

    public IHandler<T, K> SetNext(IHandler<T, K> next);

    public T Handle(K request);

}