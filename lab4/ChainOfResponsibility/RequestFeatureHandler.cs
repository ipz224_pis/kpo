namespace Lab4.ChainOfResponsibility;

public class RequestFeatureHandler : BaseUserSupportHandler
{

    public override string Handle(int request)
    {
        if (request == 2)
        {
            return $"{nameof(RequestFeatureHandler)} handled the request";
        }

        return base.Handle(request);
    }
}
