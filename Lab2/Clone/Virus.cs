using System.Text;

namespace System.Clone;
public class Virus : ICloneable
{
    public decimal Weight { get; set; }
    public int Age { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public List<Virus> Children { get; set; }

    public Virus(decimal weight, int age, string name, string type)
    {
        Weight = weight;
        Age = age;
        Name = name;
        Type = type;
        Children = new List<Virus>();
    }

    public void AddChild(Virus child)
    {
        Children.Add(child);
    }

    public object Clone()
    {
        Virus clone = new Virus(Weight, Age, Name, Type);

        foreach (var child in Children)
        {
            clone.AddChild((Virus)child.Clone());
        }

        return clone;
    }

    public override string ToString()
    {
        StringBuilder stringBuilder = new($"Name: {Name}, Type: {Type}, Weight: {Weight}, Age: {Age}");

        foreach (var child in Children)
        {
            stringBuilder.AppendFormat("{0}{1}", "\n\t\t\t", child);
        }

        return stringBuilder.ToString();
    }
}