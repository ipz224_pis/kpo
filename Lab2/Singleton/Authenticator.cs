namespace System.Singleton;
public class Authenticator
{
    private static object _lock = new();
    private static Authenticator instance;
    protected Authenticator() { }
    public static Authenticator GetInstance()
    {
        lock (_lock)
        {
            if (instance is null)
            {
                instance = new Authenticator();
            }

            return instance;
        }
    }

    public bool Authenticate(string username, string password)
    {
        return username == "Admin" && password == "2342432";
    }
}

public class AuthChild:Authenticator{
}