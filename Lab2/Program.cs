﻿using System.Builder;
using System.MethodFanctory;
using System.AbstractFanctory;
using System.Clone;
using System.Singleton;

Console.WriteLine("\n---Method factory---");

var subscriptionManagers = new List<SubscriptionManager>()
    { new ManagerCall(), new MobileApp(), new WebSite() };

subscriptionManagers.ForEach(el => Console.WriteLine(el.CalculatePlan().GetPlanInfo()));

Console.WriteLine("\n---Abstract factory---");

var deviceFactories = new List<IDeviceFactory>()
    { new BalaxyDeviceFactory(), new IProneDeviceFactory(), new KiaomiDeviceFactory() };

deviceFactories.ForEach(el =>
{
    Console.WriteLine(@$"{el.CreateLaptop().GetInfo()}; {el.CreateEBook().GetInfo()}; {el.CreateNetbook().GetInfo()}; {el.CreateSmartphone().GetInfo()}");
});

Console.WriteLine("\n ---Singleton---");

if (Authenticator.GetInstance() == AuthChild.GetInstance())
    Console.WriteLine("It is a singleton");
else
    Console.WriteLine("Hmm it is supposed to be a singleton");

Console.WriteLine("\n ---Clone---");

var grandChild1 = new Virus(0.5m, 1, "Flu-1923", "Flu");
var grandChild2 = new Virus(0.7m, 1, "Cold-2012", "Cold");
var child1 = new Virus(1.2m, 3, "COVID-19", "COVID");
child1.AddChild(grandChild1);
child1.AddChild(grandChild2);
var child2 = new Virus(0.8m, 2, "Ebola-1793", "Ebola");
var parent = new Virus(2.5m, 5, "HIV-2001", "HIV");
parent.AddChild(child1);
parent.AddChild(child2);

Console.WriteLine($"Original Virus: {parent}");

Console.WriteLine($"\nCloned Virus: {parent.Clone()}");

Console.WriteLine("\n ---Builder---");

var heroBuilder = new HeroCharacterBuilder();
var heroDirector = new CharacterDirector(heroBuilder);
heroDirector.Construct("Hero", "Human", "Male", 10, new List<string> { "Strength", "Agility" }, new List<string> { "Sword", "Shield" });
var heroCharacter = heroBuilder.Build();

Console.WriteLine($"Hero Character: \n{heroCharacter}");

var enemyBuilder = new VillainCharacterBuilder();
var enemyDirector = new CharacterDirector(enemyBuilder);
enemyDirector.Construct("Villain", "Orc", "Male", 5, new List<string> { "Brute Strength" }, new List<string> { "Club", "Hide Armor" });
var enemyCharacter = enemyBuilder.Build();

Console.WriteLine($"Villain Character: \n{enemyCharacter}");