using System.Text;

namespace System.Builder;
public abstract class Character
{
    public string Name { get; set; }
    public string Race { get; set; }
    public string Gender { get; set; }
    public int Level { get; set; }
    public List<string> Abilities { get; init; }
    public List<string> Inventory { get; init; }

    protected Character()
    {
        Abilities = new List<string>();
        Inventory = new List<string>();
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();

        if (!string.IsNullOrEmpty(Name))
            sb.AppendLine($"Name: {Name}");

        if (!string.IsNullOrEmpty(Race))
            sb.AppendLine($"Race: {Race}");

        if (!string.IsNullOrEmpty(Gender))
            sb.AppendLine($"Gender: {Gender}");

        sb.AppendLine($"Level: {Level}");

        if (!Abilities.IsEmpty())
        {
            sb.AppendLine("Abilities:");
            foreach (var ability in Abilities)
            {
                sb.AppendLine($"\t- {ability}");
            }
        }

        if (!Inventory.IsEmpty())
        {
            sb.AppendLine("Inventory:");
            foreach (var item in Inventory)
            {
                sb.AppendLine($"\t- {item}");
            }
        }

        return sb.ToString();
    }
}

public class VillainCharacter : Character
{

}

public class HeroCharacter : Character
{

}
