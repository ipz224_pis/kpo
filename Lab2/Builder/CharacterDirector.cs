namespace System.Builder;
public class CharacterDirector
{
    private ICharacterBuilder _builder;

    public CharacterDirector(ICharacterBuilder builder)
    {
        _builder = builder;
    }

    public void ChangeBuilder(ICharacterBuilder builder)
    {
        _builder = builder;
    }

    public void Construct(string name, string race, string gender, int level, List<string> abilities, List<string> inventory)
    {
        _builder.SetName(name)
                .SetRace(race)
                .SetGender(gender)
                .SetLevel(level);

        foreach (var ability in abilities)
        {
            _builder.AddAbility(ability);
        }

        foreach (var item in inventory)
        {
            _builder.AddInventoryItem(item);
        }
    }
}