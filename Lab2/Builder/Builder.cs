namespace System.Builder;
public interface ICharacterBuilder
{
    ICharacterBuilder SetName(string name);
    ICharacterBuilder SetRace(string race);
    ICharacterBuilder SetGender(string gender);
    ICharacterBuilder SetLevel(int level);
    ICharacterBuilder AddAbility(string ability);
    ICharacterBuilder AddInventoryItem(string item);
}

public class HeroCharacterBuilder : ICharacterBuilder
{
    private HeroCharacter _character = new HeroCharacter();

    public ICharacterBuilder SetName(string name)
    {
        _character.Name = name;
        return this;
    }

    public ICharacterBuilder SetRace(string race)
    {
        _character.Race = race;
        return this;
    }

    public ICharacterBuilder SetGender(string gender)
    {
        _character.Gender = gender;
        return this;
    }

    public ICharacterBuilder SetLevel(int level)
    {
        _character.Level = level;
        return this;
    }

    public ICharacterBuilder AddAbility(string ability)
    {
        _character.Abilities.Add(ability);
        return this;
    }

    public ICharacterBuilder AddInventoryItem(string item)
    {
        _character.Inventory.Add(item);
        return this;
    }

    public Character Build()
    {
        return _character;
    }
}

public class VillainCharacterBuilder : ICharacterBuilder
{
    private VillainCharacter _character = new VillainCharacter();

    public ICharacterBuilder SetName(string name)
    {
        _character.Name = name + " Dark";
        return this;
    }

    public ICharacterBuilder SetRace(string race)
    {
        _character.Race = race;
        return this;
    }

    public ICharacterBuilder SetGender(string gender)
    {
        _character.Gender = gender + " Evil";
        return this;
    }

    public ICharacterBuilder SetLevel(int level)
    {
        _character.Level = level + 10;
        return this;
    }

    public ICharacterBuilder AddAbility(string ability)
    {
        _character.Abilities.Add(ability + " (whisper: Kill all)");
        return this;
    }

    public ICharacterBuilder AddInventoryItem(string item)
    {
        _character.Inventory.Add(item + " cursed item");
        return this;
    }

    public Character Build()
    {
        return _character;
    }
}

public static class ListExtension
{

    public static bool IsEmpty<T>(this IList<T> list)
    {
        return list is null || list.Count == 0;
    }
}