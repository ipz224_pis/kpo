namespace System.MethodFanctory;
public class MobileApp : SubscriptionManager
{
    public override SubscriptionPlan CalculatePlan()
    {
        return new EducationalSubscription();
    }
}