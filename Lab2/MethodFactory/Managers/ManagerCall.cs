namespace System.MethodFanctory;
public class ManagerCall : SubscriptionManager
{
    //some storage call
    private bool HasCreditInvoces() => false;

    public override SubscriptionPlan CalculatePlan()
    {
        if (HasCreditInvoces())
        {
            return new DomesticSubscription();
        }

        return new PremiumSubscription();
    }
}