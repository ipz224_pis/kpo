namespace System.MethodFanctory;
public class WebSite : SubscriptionManager
{

    //some storage call
    private decimal GetUserBalance() => 12.3m;

    public override SubscriptionPlan CalculatePlan()
    {
        switch (GetUserBalance())
        {
            case > 12 and < 16:
                return new DomesticSubscription();
            case > 16 and < 20:
                return new EducationalSubscription();
            case > 20:
                return new PremiumSubscription();
            default:
                throw new ArgumentException("Invalid subscription type");
        }
    }
}