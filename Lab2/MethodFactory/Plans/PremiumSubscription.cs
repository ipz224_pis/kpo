using System.Text;

namespace System.MethodFanctory;
public class PremiumSubscription : SubscriptionPlan
{
    public PremiumSubscription() : base(19.99, 6, new List<string> { "Movies", "Sports", "HD" })
    {
    }

    public override string GetPlanInfo()
    {
        var result = new StringBuilder($"Premium sub:\n\tmonth fee:{MonthlyFee}\n\tminimum sub period: {MinimumSubscriptionPeriod} month\n\tChannels:\n");
        IncludedChannels.ForEach(el => result.AppendLine($"\t\t {el}"));
        return result.ToString();
    }
}