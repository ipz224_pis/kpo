using System.Text;

namespace System.MethodFanctory;
public class EducationalSubscription : SubscriptionPlan
{
    public EducationalSubscription() : base(8.99, 3, new List<string> { "Documentaries", "Educational" })
    {
    }

    public override string GetPlanInfo()
    {
        var result = new StringBuilder($"Educational sub:\n\tmonth fee:{MonthlyFee}\n\tminimum sub period: {MinimumSubscriptionPeriod} month\n\tChannels:\n");
        IncludedChannels.ForEach(el => result.AppendLine($"\t\t {el}"));
        return result.ToString();
    }
}