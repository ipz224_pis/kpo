using System.Text;

namespace System.MethodFanctory;
public class DomesticSubscription : SubscriptionPlan
{
    public DomesticSubscription():base(10.99, 1, new List<string> { "News", "Entertainment" }){
        
    }
    public override string GetPlanInfo()
    {
        var result = new StringBuilder($"Demestic sub:\n\tmonth fee:{MonthlyFee}\n\tminimum sub period: {MinimumSubscriptionPeriod} month\n\tChannels:\n");
        IncludedChannels.ForEach(el => result.AppendLine($"\t\t {el}"));
        return result.ToString();
    }
}