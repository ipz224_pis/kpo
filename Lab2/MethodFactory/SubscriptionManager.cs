namespace System.MethodFanctory;

public abstract class SubscriptionManager
{
    public abstract SubscriptionPlan CalculatePlan();
}