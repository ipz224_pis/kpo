namespace System.MethodFanctory;

public abstract class SubscriptionPlan
{

    public double MonthlyFee { get; protected set; }
    public int MinimumSubscriptionPeriod { get; protected set; }
    public List<string> IncludedChannels { get; protected set; }

    public SubscriptionPlan(double monthlyFee, int minSubscriptionPeriod, List<string> includedChannels)
    {
        MonthlyFee = monthlyFee;
        MinimumSubscriptionPeriod = minSubscriptionPeriod;
        IncludedChannels = includedChannels;
    }

    public abstract string GetPlanInfo();
}