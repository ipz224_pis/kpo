namespace System.AbstractFanctory;
public interface ILaptop
{
    string GetInfo();
}

public interface INetbook
{
    string GetInfo();
}

public interface IEBook
{
    string GetInfo();
}

public interface ISmartphone
{
    string GetInfo();
}