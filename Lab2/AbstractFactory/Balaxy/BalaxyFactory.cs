namespace System.AbstractFanctory;
public class BalaxyDeviceFactory : IDeviceFactory
{
    public ILaptop CreateLaptop()
    {
        return new BalaxyLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new BalaxyNetbook();
    }

    public IEBook CreateEBook()
    {
        return new BalaxyEBook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new BalaxySmartphone();
    }
}

public class BalaxyLaptop : ILaptop
{
    public string GetInfo()
    {
        return "Laptop Balaxy";
    }
}

public class BalaxyNetbook : INetbook
{
    public string GetInfo()
    {
        return "Netbook Balaxy";
    }
}

public class BalaxyEBook : IEBook
{
    public string GetInfo()
    {
        return "EBook Balaxy";
    }
}

public class BalaxySmartphone : ISmartphone
{
    public string GetInfo()
    {
        return "Smartphone Balaxy";
    }
}
