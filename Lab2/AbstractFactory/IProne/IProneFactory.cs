namespace System.AbstractFanctory;
public class IProneDeviceFactory : IDeviceFactory
{
    public ILaptop CreateLaptop()
    {
        return new IProneLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new IProneNetbook();
    }

    public IEBook CreateEBook()
    {
        return new IProneEBook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new IProneSmartphone();
    }
}

public class IProneLaptop : ILaptop
{
    public string GetInfo()
    {
         return "Laptop IProne";
    }
}

public class IProneNetbook : INetbook
{
    public string GetInfo()
    {
         return "Netbook IProne";
    }
}

public class IProneEBook : IEBook
{
    public string GetInfo()
    {
         return "EBook IProne";
    }
}

public class IProneSmartphone : ISmartphone
{
    public string GetInfo()
    {
        return "Smartphone IProne";
    }
}