namespace System.AbstractFanctory;
public class KiaomiDeviceFactory : IDeviceFactory
{
    public ILaptop CreateLaptop()
    {
        return new KiaomiLaptop();
    }

    public INetbook CreateNetbook()
    {
        return new KiaomiNetbook();
    }

    public IEBook CreateEBook()
    {
        return new KiaomiEBook();
    }

    public ISmartphone CreateSmartphone()
    {
        return new KiaomiSmartphone();
    }
}

public class KiaomiLaptop : ILaptop
{
    public string GetInfo()
    {
        return "Laptop Kiaomi";
    }
}

public class KiaomiNetbook : INetbook
{
    public string GetInfo()
    {
        return "Netbook Kiaomi";
    }
}

public class KiaomiEBook : IEBook
{
    public string GetInfo()
    {
        return "EBook Kiaomi";
    }
}

public class KiaomiSmartphone : ISmartphone
{
    public string GetInfo()
    {
        return "Smartphone Kiaomi";
    }
}