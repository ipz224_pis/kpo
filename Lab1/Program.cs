﻿using System.Features.Models;
using System.Repositories.Reporting;
using System.Validators.Money;
using System.Validators.Warehouse;
class Program
{
    static async Task Main()
    {
        Money money = new(13, 32, Currencies.EUR, new MoneyValidator());
        Console.WriteLine($"Total price: {money}");

        Product pr = new("Audi", money);
        pr.ReducePrice(1.3m);
        Console.WriteLine($"Total price: {money}");

        Warehouse wr = new(pr, 12, new WarehouseValidator());
        wr.BuyProduct(3);
        wr.EnrichWarehouse(1);

        Reporting reporting = new(new ReportingRepository());
        var data = (await reporting.GenerateInventoryReport(new Warehouse[] { wr }));
        var data2 = (await reporting.RegisterIncome(wr));
        var data3 = (await reporting.RegisterShipment(wr, "Zhitomyr", 8));

        Console.WriteLine("\nGenerateInventoryReport\n");
        Console.WriteLine($"data: {data.Item1}\n\npath: {data.Item2}\n\n\n");
        Console.WriteLine("\nRegisterIncome\n");
        Console.WriteLine($"data: {data2.Item1}\n\npath: {data2.Item2}\n\n\n");
        Console.WriteLine("\nRegisterShipment\n");
        Console.WriteLine($"data: {data3.Item1}\n\npath: {data3.Item2}\n\n\n");
    }
}

