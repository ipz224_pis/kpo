namespace System.Validators.Money;

public class MoneyValidator : IMoneyValidator
{
    public void ValidateCent(int value)
    {
        switch (value)
        {
            case < 0:
                throw new InvalidDataException($"Total cent is less than 0 ");
            case > 100:
                throw new InvalidDataException($"Total cent is hiegher than 100 ");
        }
    }

    public void ValidateWholePart(int value)
    {
        switch (value)
        {
            case < 0:
                throw new InvalidDataException($"Total wholePart is less than 0 ");
        }
    }

}
