namespace System.Validators.Money;
public interface IMoneyValidator
{
    void ValidateCent(int value);
    void ValidateWholePart(int value);

}