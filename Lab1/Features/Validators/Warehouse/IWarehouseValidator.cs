namespace System.Validators.Warehouse;
public interface IWarehouseValidator
{
    void ValidateQuantity(int value);

}