namespace System.Validators.Warehouse;
public class WarehouseValidator : IWarehouseValidator
{
    public void ValidateQuantity(int value)
    {
        switch (value)
        {
            case < 0:
                throw new InvalidDataException("Total quantity is less than 0");
        }

    }

}