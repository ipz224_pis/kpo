using System.Validators.Warehouse;

namespace System.Features.Models;

public class Warehouse
{
    private int _quantity;
    private IWarehouseValidator _warehouseValidator;

    public Product Unit { get; init; }
    public int Quantity
    {
        get
        {
            return _quantity;
        }
        private set
        {
            _warehouseValidator.ValidateQuantity(value);
            _quantity = value;
        }
    }
    public DateTime LastArrivalDate { get; private set; }

    public Warehouse(Product unit, int quantity, IWarehouseValidator warehouseValidator, DateTime? lastArrivalDate = null)
    {
        _warehouseValidator = warehouseValidator;

        Unit = unit;
        Quantity = quantity;
        LastArrivalDate = lastArrivalDate ?? DateTime.UtcNow;
    }

    public void BuyProduct(int quantity)
    {
        Quantity -= quantity;
    }

    public void EnrichWarehouse(int quantity)
    {
        Quantity += quantity;
        LastArrivalDate = DateTime.UtcNow;
    }
}