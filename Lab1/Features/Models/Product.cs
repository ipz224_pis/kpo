namespace System.Features.Models;
public class Product
{
    public string Name { get; set; }
    public Money Price { get; set; }

    public Product(string name, Money price)
    {
        Name = name;
        Price = price;
    }

    public void ReducePrice(decimal amount)
    {
        decimal whole = Math.Truncate(amount);
        decimal fractional = amount - whole;

        Price.WholePart -= (int)whole;
        Price.Cents -= (int)(fractional * 100);

    }
}
