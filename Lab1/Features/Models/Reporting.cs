using System.Repositories.Reporting;
using System.Text;

namespace System.Features.Models;
public class Reporting
{
    private readonly IReportingRepository _reporting;

    public Reporting(IReportingRepository reporting)
    {
        _reporting = reporting;
    }

    public async Task<(string, string)> RegisterIncome(Warehouse warehouse)
    {
        string register = $"Registered incoming goods: {warehouse} units of {warehouse.Unit.Name} at {warehouse.Unit.Price.GetTotalPrice()} {warehouse.Unit.Price.Currency.ToString()} per unit.\n";
        return (register, await _reporting.SaveIncome(register));
    }

    public async Task<(string, string)> RegisterShipment(Warehouse warehouse, string shipmentAddress, int quantity)
    {
        warehouse.BuyProduct(quantity);
        Guid orderId = Guid.NewGuid();
        decimal pricePerUnit = warehouse.Unit.Price.GetTotalPrice();

        string shipmentInfo = @$"
            Order #{orderId}
            Shipment Address: {shipmentAddress}
            Quentity: {quantity}
            Price per unit: {pricePerUnit} {warehouse.Unit.Price.Currency.ToString()}
            -------------------------------------------------
            TotalPrice: {(pricePerUnit * quantity)} {warehouse.Unit.Price.Currency.ToString()}
        ";

        return (shipmentInfo, await _reporting.SaveShipment(shipmentInfo, orderId));
    }

    public async Task<(string, string)> GenerateInventoryReport(IEnumerable<Warehouse> warehouses)
    {

        StringBuilder report = new StringBuilder("Name,Quantity,LastArrivalDate,Price,Currency\n");

        foreach (var entry in warehouses)
        {
            report.AppendFormat("{0},{1},{2},{3},{4}\n",
             entry.Unit.Name, entry.Quantity, entry.LastArrivalDate,
             entry.Unit.Price.GetTotalPrice(), entry.Unit.Price.Currency.ToString());
        }

        var data = report.ToString();

        return (data, await _reporting.SaveInventoryReport(data));
    }
}