using System.Validators.Money;

namespace System.Features.Models;

public class Money
{
    private int _wholePart;
    private int _cents;
    private IMoneyValidator _validator;

    public int WholePart
    {
        get
        {
            return _wholePart;
        }
        set
        {
            _validator.ValidateWholePart(value);
            _wholePart = value;
        }
    }
    public Currencies Currency { get; init; }
    public int Cents
    {
        get
        {
            return _cents;
        }
        set
        {
            _validator.ValidateCent(value);
            _cents = value;
        }
    }

    public Money(int wholePart, int cents, Currencies currency, IMoneyValidator validator)
    {
        Currency = currency;
        _wholePart = wholePart;
        _cents = cents;
        _validator = validator;
    }

    public decimal GetTotalPrice(){
        return _wholePart + _cents * 0.01m;
    }

    //I do not think that print method must be in that class
    public override string ToString()
    {
        return $"{WholePart}.{Cents:D2} {Currency.ToString()}";
    }
}
