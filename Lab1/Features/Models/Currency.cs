namespace System.Features.Models;

public static class CurrencyToStringExtension
{
    public static string ToString(this Currencies currency)
    {
        switch (currency)
        {
            case Currencies.EUR:
                return "Euro";
            case Currencies.UAH:
                return "Hryvnya";
            case Currencies.USD:
                return "Dollar";
        }

        return "no-way";
    }

}

public enum Currencies
{
    USD,
    UAH,
    EUR,
}