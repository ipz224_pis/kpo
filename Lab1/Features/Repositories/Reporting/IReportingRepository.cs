namespace System.Repositories.Reporting;

public interface IReportingRepository
{
    Task<string> SaveIncome(string data);
    Task<string> SaveShipment(string data, Guid orderId);
    Task<string> SaveInventoryReport(string data);

}