namespace System.Repositories.Reporting;

public class ReportingRepository : IReportingRepository
{
    static readonly string _root = AppDomain.CurrentDomain.BaseDirectory;
    static readonly string _inventoryReportFileName = "InventoryReport";
    static readonly string _productIncomes = "ProductIncomes.txt";
    static readonly string _shipment = "Shipment";

    public async Task<string> SaveIncome(string data)
    {
        string path = Path.Combine(_root, _productIncomes);

        if (!File.Exists(path))
        {
            using (var file = File.CreateText(path))
            {
                await file.WriteLineAsync(data);
            }
            return path;
        }

        using (StreamWriter writer = File.AppendText(path))
        {
            await writer.WriteLineAsync(data);
        }

        return path;
    }
    public async Task<string> SaveShipment(string data, Guid orderId)
    {
        string path = Path.Combine(_root, $"{_shipment} #{orderId}.txt");

        using (var file = File.CreateText(path))
        {
            await file.WriteLineAsync(data);
        }

        return path;
    }
    public async Task<string> SaveInventoryReport(string data)
    {

        string path = Path.Combine(_root, $"{_inventoryReportFileName}-{DateTime.UtcNow.Ticks}.csv");

        using (var file = File.CreateText(path))
        {
            await file.WriteAsync(data);
        }

        return path;
    }
}