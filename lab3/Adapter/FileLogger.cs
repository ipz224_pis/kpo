public class FileLogger
{
    private readonly string filePath;

    public FileLogger(string filePath)
    {
        this.filePath = filePath;
    }

    public void Write(string message)
    {
        using (StreamWriter writer = new StreamWriter(filePath, true))
        {
            writer.Write(message);
        }
    }

    public void WriteLine(string message)
    {
        using (StreamWriter writer = new StreamWriter(filePath, true))
        {
            writer.WriteLine(message);
        }
    }
}