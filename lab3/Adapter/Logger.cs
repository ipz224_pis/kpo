public class Logger
{
    public virtual void Log(string message)
    {
        WriteMessage(message, ConsoleColor.DarkGreen);
    }

    public virtual void Error(string message)
    {
        WriteMessage(message, ConsoleColor.DarkRed);
    }

    public virtual void Warn(string message)
    {
        WriteMessage(message, ConsoleColor.Yellow);
    }

    private void WriteMessage(string message, ConsoleColor color)
    {
        Console.ForegroundColor = color;
        Console.WriteLine(message);
        Console.ResetColor();
    }
}