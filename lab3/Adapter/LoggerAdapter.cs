
public class LoggerAdapter : Logger
{

    private readonly FileLogger fileLogger;
    public LoggerAdapter(FileLogger fileLogger)
    {
        this.fileLogger = fileLogger;
    }

    public override void Log(string message)
    {
        fileLogger.WriteLine("[INFO]\n" + message);
    }

    public override void Error(string message)
    {
        fileLogger.WriteLine("[ERROR]\n" + message);
    }

    public override void Warn(string message)
    {
        fileLogger.WriteLine("[WARN]\n" + message);
    }
}