public class Hero
{
    private string name;
    private HeroClass heroClass;
    private IInventory inventory;

    public Hero(string name, HeroClass heroClass, IInventory inventory)
    {
        this.name = name;
        this.heroClass = heroClass;
        this.inventory = inventory;
    }

    public override string ToString()
    {
        return @$"Name: {name}
                 Class: {heroClass.ToString()}
                 Inventory Data: 
                 {inventory.GetInventoryInfo()}";
    }

}

public enum HeroClass
{
    Mage,
    Palldin,
    Warrior,
}

public static class HeroClassExtension
{
    public static string ToString(this HeroClass heroClass) => heroClass switch
    {
        HeroClass.Mage => "Mage",
        HeroClass.Palldin => "Palldin",
        HeroClass.Warrior => "Warrior",
        _ => throw new InvalidDataException($"Unccorect value of {nameof(HeroClass)} type - {heroClass}")
    };
}