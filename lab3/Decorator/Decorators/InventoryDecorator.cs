public class BaseInventoryDecorator : IInventory
{
    private IInventory inventory;
    public BaseInventoryDecorator(IInventory inventory)
    {
        this.inventory = inventory;
    }

    public virtual string GetInventoryInfo()
    {
        return inventory.GetInventoryInfo();
    }
}