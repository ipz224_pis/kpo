public class ArtifactDecorator : BaseInventoryDecorator
{
    private string artifactsData;
    public ArtifactDecorator(IInventory inventory, string artifactsData) : base(inventory)
    {
        this.artifactsData = artifactsData;
    }
    public override string GetInventoryInfo()
    {
        return base.GetInventoryInfo() + $"\n Artifacts Data: \n{artifactsData}\n";
    }
}