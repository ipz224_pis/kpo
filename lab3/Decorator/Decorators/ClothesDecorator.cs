public class ClothesDecorator : BaseInventoryDecorator
{
    private string clothesData;
    public ClothesDecorator(IInventory inventory, string clothesData) : base(inventory)
    {
        this.clothesData = clothesData;
    }

    public override string GetInventoryInfo()
    {
        return base.GetInventoryInfo() + $"\n Clothes Data: \n{clothesData}\n";
    }
}