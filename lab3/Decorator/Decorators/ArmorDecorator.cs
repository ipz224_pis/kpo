public class ArmorDecorator : BaseInventoryDecorator
{
    private string armorData;
    public ArmorDecorator(IInventory inventory, string armorData) : base(inventory)
    {
        this.armorData = armorData;
    }
    public override string GetInventoryInfo()
    {
        return base.GetInventoryInfo() + $"\n Armor Data: \n{armorData}\n";
    }
}