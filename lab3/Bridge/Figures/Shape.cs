public abstract class Shape
{
    private readonly IRendereManager renderer;
    private readonly string name;

    public Shape(IRendereManager renderer, string name)
    {
        this.renderer = renderer;
        this.name = name;
    }

    public void Draw()
    {
        renderer.Render(name ?? this.GetType().Name);
    }
}