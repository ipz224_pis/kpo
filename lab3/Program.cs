﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Running;

[MemoryDiagnoser]
public class TextHttpConverterBenchmark
{
    private readonly TextHttpConverter _converter;
    private readonly IFormatRuler _formater;
    private readonly string _url;
    private readonly Consumer _consumer;

    public TextHttpConverterBenchmark()
    {
        _converter = TextHttpConverter.Instance;
        _formater = new FormatRuler();
        _url = "https://www.gutenberg.org/cache/epub/1513/pg1513.txt";
        _consumer = new();
    }

    [Benchmark]
    public void WithoutCache()
    {
        _converter.Load(_url, _formater, skiCache: true).GetAwaiter().GetResult().Consume(_consumer);
    }

    [Benchmark]
    public void WithCache()
    {
        _converter.Load(_url, _formater).GetAwaiter().GetResult().Consume(_consumer);
    }
}

public class Program
{
    public static async Task Main(string[] args)
    {
        Console.WriteLine("\n-----Adapter------\n");

        var logger = new Logger();
        var fileWriter = new FileLogger(Path.Combine(Directory.GetCurrentDirectory(), "Log.txt"));

        var fileLoggerAdapter = new LoggerAdapter(fileWriter);

        logger.Log("This is a regular log message");
        logger.Error("This is an error message");
        logger.Warn("This is a warning message");

        fileLoggerAdapter.Log("This is a log message written to a file");
        fileLoggerAdapter.Error("This is an error message written to a file");
        fileLoggerAdapter.Warn("This is a warning message written to a file");

        Console.WriteLine("\n-----Bridge------\n");

        IRendereManager vectorRenderer = new VectorRenderer();
        IRendereManager rasterRenderer = new RasterRenderer();

        var circle = new Circle(vectorRenderer);
        circle.Draw();

        var square = new Square(rasterRenderer);
        square.Draw();

        var triangle = new Triangle(vectorRenderer);
        triangle.Draw();


        Console.WriteLine("\n-----Proxy------\n");

        string filePath = "locked-data.txt";
        var url = "https://www.gutenberg.org/cache/epub/1513/pg1513.txt";

        using (HttpClient client = new HttpClient())
        {
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            File.WriteAllText(filePath, result);

            Console.WriteLine("Text from the book has been written to sample.txt.");
        }

        var reader = new SmartTextReader(filePath);

        var checkerProxy = new SmartTextReaderLogger(reader);
        char[][] content = checkerProxy.ReadText();
        if (content != null)
        {
            foreach (var line in content)
            {
                Console.WriteLine(line);
            }
        }

        SmartTextReaderLocker limitedReader = new SmartTextReaderLocker(reader, @".*locked.*");
        char[][] limitedContent = limitedReader.ReadText();
        if (limitedContent != null)
        {
            foreach (var line in limitedContent)
            {
                Console.WriteLine(line);
            }
        }

        Console.WriteLine("\n-----Composite------\n");

        var children = new List<LightNode>
        {
            new LightElementNode("div", "block", true, new List<string>{"container"}, new List<LightNode>
            {
                new LightElementNode("h1", "block", true, new List<string>(), new List<LightNode>
                {
                    new LightTextNode("Welcome to LightHTML!")
                }),
                new LightElementNode("p", "block", true, new List<string>(), new List<LightNode>
                {
                    new LightTextNode("This is a simple example of LightHTML.")
                }),
                new LightElementNode("ul", "block", true, new List<string>(), new List<LightNode>
                {
                    new LightElementNode("li", "block", true, new List<string>(), new List<LightNode>
                    {
                        new LightTextNode("Item 1")
                    }),
                    new LightElementNode("li", "block", true, new List<string>(), new List<LightNode>
                    {
                        new LightTextNode("Item 2")
                    }),
                    new LightElementNode("li", "block", true, new List<string>(), new List<LightNode>
                    {
                        new LightTextNode("Item 3")
                    })
                })
            })
        };

        var root = new LightElementNode("html", "block", true, new List<string>(), children);
        Console.WriteLine(root.OuterHTML);

        Console.WriteLine("\n-----Decorator------\n");

        IInventory baseInventory = new BaseInventoryDecorator(new Inventory());
        IInventory clothesDecorator = new ClothesDecorator(baseInventory, "Leather armor");
        IInventory armorDecorator = new ArmorDecorator(clothesDecorator, "Steel helmet");
        IInventory artifactDecorator = new ArtifactDecorator(armorDecorator, "Magic staff");

        var hero = new Hero("Gandalf", HeroClass.Mage, artifactDecorator);

        Console.WriteLine(hero);

        Console.WriteLine("\n-----Flyweight------\n");
        BenchmarkRunner.Run<TextHttpConverterBenchmark>();
    }
}