public interface IVisitor
{
    void VisitText(LightTextNode node);

    void VisitElement(LightElementNode node);
}