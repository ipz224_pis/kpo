public class CountOfChildrenVisitor : IVisitor
{
    public void VisitText(LightTextNode node)
    {
        Console.WriteLine("\n-- Text node: \n");
        Console.WriteLine("Count of children: 0");
    }

    public void VisitElement(LightElementNode node)
    {
        Console.WriteLine("\n-- Element node: \n");
        Console.WriteLine("Count of children: " + node.Count());
    }
}