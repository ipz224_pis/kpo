public abstract class InputState<T>
{
    protected InputLightNode<T> _context;

    public void SetContext(InputLightNode<T> context)
    {
        this._context = context;
    }

    public abstract T Read(T data);

    public abstract T Write(T data);
}
