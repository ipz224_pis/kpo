
public class NormalInputState<T> : InputState<T>
{
    public override T Read(T data)
    {
        return data;
    }

    public override T Write(T data)
    {
        return data;
    }
}