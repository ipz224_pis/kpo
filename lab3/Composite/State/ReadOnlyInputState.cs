
public class ReadOnlyInputState<T> : InputState<T>
{
    public override T Read(T value)
    {
        return value;
    }

    public override T Write(T _)
    {
        throw new Exception("State of input is readonly");
    }
}
