public class WriteOnlyInputState<T> : InputState<T>
{
    public override T Read(T _)
    {
        throw new Exception("State of input is writeonly");
    }

    public override T Write(T data)
    {
        return data;
    }
}
