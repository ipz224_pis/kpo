
public class LightTextNode : LightNode
{
    public string Text { get; }
    public LightTextNode(string text)
    {
        Text = text;
    }

    protected override string PostRender()
    {
        return "</text>";
    }

    protected override string PreRender()
    {
        return "<text>";
    }

    protected override string RenderContent()
    {
        return Text;
    }

    public override string ToString()
    {
        return Text;
    }

    public override void Accept(IVisitor visitor)
    {
        visitor.VisitText(this);
    }
}