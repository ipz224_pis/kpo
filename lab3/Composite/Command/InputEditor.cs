public class InputEditor<T>
{
    private readonly InputLightNode<T> input;
    public InputEditor(InputLightNode<T> input)
    {
        this.input = input;
    }
    public void Write(T data)
    {
        new ChangeInputText<T>(input, data, new WriteOnlyInputState<T>()).Execute();
    }
}