public class ChangeInputText<T> : ICommand
{
    private readonly InputLightNode<T> input;
    private readonly T data;
    private readonly InputState<T> state;

    public ChangeInputText(InputLightNode<T> input, T data, InputState<T> state = null)
    {
        this.data = data;
        this.input = input;
        this.state = state;
    }

    public void Execute()
    {
        if (state is not null)
            input.State = state;

        input.Content = data;
    }
}

