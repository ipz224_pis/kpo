
public abstract class LightNode
{
    public string Render()
    {
        return PreRender() +
               RenderContent() +
               PostRender();
    }

    protected abstract string RenderContent();

    protected virtual string PreRender() => "";

    protected virtual string PostRender() => "";

    public abstract void Accept(IVisitor visitor);
}