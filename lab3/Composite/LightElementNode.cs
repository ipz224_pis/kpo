using System.Text;
using System.Collections;
public class LightElementNode : LightNode, IEnumerable<LightNode>
{
    public string TagName { get; }
    public string DisplayType { get; }
    public bool IsClosed { get; }
    public IEnumerable<string> Classes { get; }
    public IEnumerable<LightNode> Children { get; }
    public IDictionary<string, string> Attributes { get; }

    public LightElementNode(string tagName, string displayType, bool isClosed, List<string> classes, List<LightNode> children)
    {
        TagName = tagName;
        DisplayType = displayType;
        IsClosed = isClosed;
        Classes = classes;
        Children = children;
        Attributes = new Dictionary<string, string>();
    }

    public string OuterHTML
    {
        get
        {
            StringBuilder html = new StringBuilder();
            html.Append($"<{TagName}");

            if (Classes is not null && Classes.Any())
            {
                html.Append(" class=\"");
                html.Append(string.Join(" ", Classes));
                html.Append('"');
            }

            foreach (var el in Attributes)
                html.Append($" ${el.Key}=\"{el.Value}\" ");

            if (IsClosed)
            {
                html.Append('>');
                if (Children is not null && Children.Any())
                {
                    foreach (var child in Children)
                    {
                        html.Append(child.Render());
                    }
                }
                html.Append($"</{TagName}>");
            }
            else
            {
                html.Append("/>");
            }

            return html.ToString();
        }
    }

    public string InnerHTML
    {
        get
        {
            var html = new StringBuilder();
            foreach (var child in Children)
            {
                html.Append(child.Render());
            }
            return html.ToString();
        }
    }

    protected override string RenderContent()
    {
        return OuterHTML;
    }

    public override string ToString()
    {
        return Render();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return Children.GetEnumerator();
    }

    public IEnumerator<LightNode> GetEnumerator()
    {
        return Children.GetEnumerator();
    }
    public override void Accept(IVisitor visitor)
    {
        visitor.VisitElement(this);
    }
}
