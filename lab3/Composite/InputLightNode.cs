public class InputLightNode<T> : LightElementNode
{
    public InputState<T> State { private get; set; }

    private T content;

    public T Content
    {
        get
        {
            return State.Read(content);
        }
        set
        {
            content = State.Write(value);
        }
    }

    public InputLightNode(string tagName, string displayType)
    : base(tagName, displayType, false, new(), new())
    { }

    protected override string RenderContent()
    {
        Attributes.Add("value", content.ToString());
        return base.RenderContent();
    }
}