using System.Collections.Concurrent;

public sealed class TextHttpConverter
{
    private ConcurrentDictionary<string, LightNode> cache = new();

    public static TextHttpConverter Instance { get; } = new();

    private TextHttpConverter()
    { }

    public async Task<IEnumerable<LightNode>> Load(string url, IFormatRuler formater, bool skiCache = false)
    {
        using var httpClient = new HttpClient();

        var nodesResult = new List<LightNode>();

        var data = (await httpClient.GetStringAsync(url)).Split('\n');

        var index = 0;
        foreach (var str in data)
        {
            if (string.IsNullOrWhiteSpace(str) || string.IsNullOrEmpty(str))
            {
                continue;
            }

            if (skiCache)
            {
                nodesResult.Add(formater.Format(str, index));
                continue;
            }

            if (cache.TryGetValue(str, out LightNode value))
            {
                nodesResult.Add(value);
            }
            else
            {
                cache[str] = formater.Format(str, index);
                nodesResult.Add(cache[str]);
            }
            index++;
        }

        return nodesResult;
    }

}