public interface IFormatRuler
{
    LightNode Format(string data, int index);
}