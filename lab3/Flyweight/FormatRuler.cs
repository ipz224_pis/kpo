using System.Text.RegularExpressions;

public class FormatRuler : IFormatRuler
{
    public LightNode Format(string data, int index)
    {
        if (index == 0)
        {
            return new LightElementNode("h1", "inline", true, null, new List<LightNode>() { new LightTextNode(data) });
        }

        if (data.Length < 20)
        {
            return new LightElementNode("h2", "inline", true, null, new List<LightNode>() { new LightTextNode(data) });
        }

        if (data.StartsWith(@"\s"))
        {
            return new LightElementNode("blockquote", "block", true, null, new List<LightNode>() { new LightTextNode(data) });
        }

        return new LightElementNode("p", "inline", true, null, new List<LightNode>() { new LightTextNode(data) });
    }
}