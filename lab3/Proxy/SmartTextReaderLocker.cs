using System.Text.RegularExpressions;

public class SmartTextReaderLocker
{
    private SmartTextReader Reader;
    private Regex Pattern;

    public SmartTextReaderLocker(SmartTextReader reader, string pattern)
    {
        Reader = reader;
        Pattern = new Regex(pattern);
    }

    public char[][] ReadText()
    {
        if (Pattern.IsMatch(Reader.FilePath))
        {
            Console.WriteLine("Access denied!");
            return null;
        }
        else
        {
            return Reader.ReadText();
        }
    }
}
