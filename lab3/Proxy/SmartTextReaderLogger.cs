public class SmartTextReaderLogger : IFileReader
{
    private SmartTextReader Reader;

    public SmartTextReaderLogger(SmartTextReader reader)
    {
        Reader = reader;
    }

    public char[][] ReadText()
    {
        Console.WriteLine("Opening file...");
        char[][] textArray = Reader.ReadText();
        Console.WriteLine("File successfully read.");
        Console.WriteLine($"Number of lines: {textArray.Length}");
        Console.WriteLine($"Number of characters: {CountCharacters(textArray)}");
        Console.WriteLine("Closing file...");
        return textArray;
    }

    private int CountCharacters(char[][] textArray)
    {
        int count = 0;
        foreach (char[] line in textArray)
        {
            count += line.Length;
        }
        return count;
    }
}
