public class SmartTextReader : IFileReader
{
    public string FilePath { get; init; }

    public SmartTextReader(string filePath)
    {
        FilePath = filePath;
    }

    public char[][] ReadText()
    {
        string[] lines = File.ReadAllLines(FilePath);
        char[][] textArray = new char[lines.Length][];
        for (int i = 0; i < lines.Length; i++)
        {
            textArray[i] = lines[i].ToCharArray();
        }
        return textArray;
    }
}