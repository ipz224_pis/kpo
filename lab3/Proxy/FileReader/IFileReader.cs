public interface IFileReader
{
    char[][] ReadText();
}